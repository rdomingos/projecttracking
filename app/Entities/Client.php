<?php

namespace rrd\Entities;

use Illuminate\Database\Eloquent\Model;

class Client extends Model
{
      protected $fillable = [
            'name',
            'responsible',
            'email',
            'phone',
            'address',
            'obs'
      ];

      public function projects()
      {
            return $this->hasMany(Project::class);
      }

      public function memberOf()
      {
            return $this->belongsToMany(Project::class, 'project_members', 'user_id', 'project_id');
      }
}
