<?php
/**
 * Created by PhpStorm.
 * User: rafar
 * Date: 05/07/2016
 * Time: 21:20
 */

namespace rrd\Entities;

use Illuminate\Database\Eloquent\Model;


class Project extends Model
{
    protected $fillable = [
        'owner_id',
        'client_id',
        'name',
        'descrition',
        'progress',
        'status',
        'due_date'
    ];

    public function client()
    {
        return $this->belongsTo(Client::class);
    }

    public function owner()
    {
        return $this->belongsTo(User::class);
    }

    public function notes()
    {
        return $this->hasMany(ProjectNote::class);
    }

    public function tasks()
    {
        return $this->hasMany(ProjectTask::class);
    }

    public function members()
    {
        return $this->belongsToMany(User::class, 'project_members', 'project_id', 'user_id')->withTimestamps();
    }

    public function hasMembers($membros)
    {
        return ($this->members()->wherePivotIn('user_id', $membros)->select('user_id')->getResults()->toArray());
    }
}