<?php

namespace rrd\Http\Controllers;

use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use rrd\Http\Requests;
use rrd\Repositories\ClientRepository;
use rrd\Services\ClientService;

/**
 * Class ClientController
 * @package rrd\Http\Controllers
 */
class ClientController extends Controller
{
    /**
     * @var
     */
    private $repository;
    /**
     * @var
     */
    private $service;

    /**
     * ClientController constructor.
     * @param ClientRepository $repository
     * @param ClientService $service
     */
    public function __construct(ClientRepository $repository, ClientService $service)
    {
        $this->repository = $repository;
        $this->service = $service;
    }

    /**
     * @return mixed
     */
    public function index()
    {
        return $this->repository->with('projects')->all();
    }

    /**
     * @param $id
     * @return array|mixed
     */
    public function show($id)
    {
        try {
            return $this->repository->with(['projects'])->find($id);
        } catch (ModelNotFoundException $e) {
            return ['error' => true, 'message' => "Dados de Cliente não encontrado."];
        }
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function store(Request $request)
    {
        return $this->service->create($request->all());
    }

    /**
     * @param Request $request
     * @param $id
     * @return array|mixed
     */
    public function update(Request $request, $id)
    {
        return $this->service->update($request->all(), $id);
    }

    /**
     * @param $id
     * @return array
     */
    public function destroy($id)
    {
        return $this->service->delete($id);
    }
}
