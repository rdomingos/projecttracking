<?php
/**
 * Created by PhpStorm.
 * User: rafar
 * Date: 29/07/2016
 * Time: 23:19
 */

namespace rrd\Http\Controllers;

use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use rrd\Http\Requests;
use rrd\Repositories\ProjectRepository;
use rrd\Services\ProjectService;

/**
 * Class ProjectController
 * @package rrd\Http\Controllers
 */
class ProjectController extends Controller
{
    /**
     * @var
     */
    private $repository;
    /**
     * @var
     */
    private $service;

    /**
     * ProjectController constructor.
     * @param ProjectRepository $repository
     * @param ProjectService $service
     */
    public function __construct(ProjectRepository $repository, ProjectService $service)
    {
        $this->repository = $repository;
        $this->service = $service;
    }

    /**
     * @return mixed
     */
    public function index()
    {
        return $this->repository->with(['client', 'owner'])->all();
    }

    /**
     * @param $id
     * @return array|mixed
     */
    public function show($id)
    {
        try {
            return $this->repository->with(['client', 'owner', 'notes', 'members'])->find($id);
        } catch (ModelNotFoundException $e) {
            return ['error' => true, 'message' => "Dados de Projeto não encontrado."];
        }
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function store(Request $request)
    {
        return $this->service->create($request->all());
    }

    /**
     * @param Request $request
     * @param $id
     * @return array|mixed
     */
    public function update(Request $request, $id)
    {
        return $this->service->update($request->all(), $id);
    }

    public function addMembers(Request $request, $idProjeto)
    {
        return $this->service->addMembers($request->all(), $idProjeto, false);
    }

    public function addMembersSync(Request $request, $idProjeto)
    {
        return $this->service->addMembers($request->all(), $idProjeto, true);
    }

    public function removeMember(Request $request, $idProjeto)
    {
        return $this->service->removeMember($request->all(), $idProjeto);
    }

    public function members($idProjeto)
    {
        try {
            return $this->repository->find($idProjeto)->members;
        } catch (ModelNotFoundException $e) {
            return ['error' => true, 'message' => "Dados de Projeto não encontrado."];
        }
    }

    /**
     * @param $id
     * @return array
     */
    public function destroy($id)
    {
        return $this->service->delete($id);
    }
}