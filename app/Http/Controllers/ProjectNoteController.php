<?php
/**
 * Created by PhpStorm.
 * User: rafar
 * Date: 29/07/2016
 * Time: 23:19
 */

namespace rrd\Http\Controllers;

use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use rrd\Http\Requests;
use rrd\Repositories\ProjectNoteRepository;
use rrd\Services\ProjectNoteService;

/**
 * Class ProjectController
 * @package rrd\Http\Controllers
 */
class ProjectNoteController extends Controller
{
    /**
     * @var
     */
    private $repository;
    /**
     * @var
     */
    private $service;

    /**
     * ProjectController constructor.
     * @param ProjectNoteRepository $repository
     * @param ProjectNoteService $service
     */
    public function __construct(ProjectNoteRepository $repository, ProjectNoteService $service)
    {
        $this->repository = $repository;
        $this->service = $service;
    }

    /**
     * @param $id
     * @return mixed
     */
    public function index($id)
    {
        return $this->repository->findWhere(['project_id' => $id]);
    }

    /**
     * @param $id
     * @return array|mixed
     */
    public function show($id, $note_id)
    {
        try {
            return $this->repository->findWhere(['project_id' => $id, 'id' => $note_id]);
        } catch (ModelNotFoundException $e) {
            return ['error' => true, 'message' => "Dados de Notas não encontrado."];
        }
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function store(Request $request)
    {
        return $this->service->create($request->all());
    }

    /**
     * @param Request $request
     * @param $id
     * @param $note_id
     * @return array|mixed
     */
    public function update(Request $request, $id, $note_id)
    {
        return $this->service->update($request->all(), $id, $note_id);
    }

    /**
     * @param $id
     * @param $note_id
     * @return array
     */
    public function destroy($id, $note_id)
    {
        return $this->service->delete($id, $note_id);
    }
}