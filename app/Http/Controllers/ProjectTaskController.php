<?php
/**
 * Created by PhpStorm.
 * User: rafar
 * Date: 29/07/2016
 * Time: 23:19
 */

namespace rrd\Http\Controllers;

use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use rrd\Repositories\ProjectTaskRepository;
use rrd\Services\ProjectTaskService;

/**
 * Class ProjectController
 * @package rrd\Http\Controllers
 */
class ProjectTaskController extends Controller
{
    /**
     * @var
     */
    private $repository;
    /**
     * @var
     */
    private $service;

    /**
     * ProjectController constructor.
     * @param ProjectTaskRepository $repository
     * @param ProjectTaskService $service
     */
    public function __construct(ProjectTaskRepository $repository, ProjectTaskService $service)
    {
        $this->repository = $repository;
        $this->service = $service;
    }

    /**
     * @param $id
     * @return mixed
     */
    public function index($id)
    {
        return $this->repository->findWhere(['project_id' => $id]);
    }

    /**
     * @param $id
     * @return array|mixed
     */
    public function show($id, $task_id)
    {
        try {
            return $this->repository->findWhere(['project_id' => $id, 'id' => $task_id]);
        } catch (ModelNotFoundException $e) {
            return ['error' => true, 'message' => "Dados de Tarefa não encontrados."];
        }
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function store(Request $request)
    {
        return $this->service->create($request->all());
    }

    /**
     * @param Request $request
     * @param $id
     * @param $task_id
     * @return array|mixed
     */
    public function update(Request $request, $id, $task_id)
    {
        return $this->service->update($request->all(), $id, $task_id);
    }

    /**
     * @param $id
     * @param $task_id
     * @return array
     */
    public function destroy($id, $task_id)
    {
        return $this->service->delete($id, $task_id);
    }
}