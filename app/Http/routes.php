<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::group(['prefix' => 'clients'], function () {
    Route::get('', ['uses' => 'ClientController@index', 'as' => 'products.index']);
    Route::post('', ['uses' => 'ClientController@store', 'as' => 'products.store']);
    Route::put('{id}', ['uses' => 'ClientController@update', 'as' => 'products.update']);
    Route::get('{id}', ['uses' => 'ClientController@show', 'as' => 'products.show']);
    Route::delete('{id}', ['uses' => 'ClientController@destroy', 'as' => 'products.destroy']);
});


Route::group(['prefix' => 'projects'], function () {
    //NOTAS
    Route::get('{id}/notes', ['uses' => 'ProjectNoteController@index', 'as' => 'projects.notes.index']);
    Route::get('{id}/notes/{note_id}', ['uses' => 'ProjectNoteController@show', 'as' => 'projects.notes.show']);
    Route::put('{id}/notes/{note_id}', ['uses' => 'ProjectNoteController@update', 'as' => 'projects.notes.update']);
    Route::post('{id}/notes', ['uses' => 'ProjectNoteController@store', 'as' => 'projects.notes.store']);
    Route::delete('{id}/notes/{note_id}', ['uses' => 'ProjectNoteController@destroy', 'as' => 'projects.notes.destroy']);
    //TASKS
    Route::get('{id}/tasks', ['uses' => 'ProjectTaskController@index', 'as' => 'projects.tasks.index']);
    Route::get('{id}/tasks/{note_id}', ['uses' => 'ProjectTaskController@show', 'as' => 'projects.tasks.show']);
    Route::put('{id}/tasks/{note_id}', ['uses' => 'ProjectTaskController@update', 'as' => 'projects.tasks.update']);
    Route::post('{id}/tasks', ['uses' => 'ProjectTaskController@store', 'as' => 'projects.tasks.store']);
    Route::delete('{id}/tasks/{note_id}', ['uses' => 'ProjectTaskController@destroy', 'as' => 'projects.tasks.destroy']);

    //PROJETOS
    Route::get('', ['uses' => 'ProjectController@index', 'as' => 'projects.index']);
    Route::get('{id}', ['uses' => 'ProjectController@show', 'as' => 'projects.show']);
    Route::post('', ['uses' => 'ProjectController@store', 'as' => 'projects.store']);
    Route::put('{id}', ['uses' => 'ProjectController@update', 'as' => 'projects.update']);
        Route::put('{id}/addMembers', ['uses' => 'ProjectController@addMembers', 'as' => 'projects.addMembers']);
        Route::put('{id}/addMembersSync', ['uses' => 'ProjectController@addMembersSync', 'as' => 'projects.addMembersSync']);
        Route::put('{id}/removeMember', ['uses' => 'ProjectController@removeMember', 'as' => 'projects.removeMember']);
        Route::get('{id}/members', ['uses' => 'ProjectController@members', 'as' => 'projects.members']);
    Route::delete('{id}', ['uses' => 'ProjectController@destroy', 'as' => 'projects.destroy']);
});