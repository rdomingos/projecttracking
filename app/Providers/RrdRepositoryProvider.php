<?php

namespace rrd\Providers;

use Illuminate\Support\ServiceProvider;

class RrdRepositoryProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(\rrd\Repositories\ClientRepository::class, \rrd\Repositories\ClientRepositoryEloquent::class);
        $this->app->bind(\rrd\Repositories\ProjectRepository::class, \rrd\Repositories\ProjectRepositoryEloquent::class);
        $this->app->bind(\rrd\Repositories\ProjectNoteRepository::class, \rrd\Repositories\ProjectNoteRepositoryEloquent::class);
        $this->app->bind(\rrd\Repositories\ProjectTaskRepository::class, \rrd\Repositories\ProjectTaskRepositoryEloquent::class);
    }
}
