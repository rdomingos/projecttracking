<?php
/**
 * Created by PhpStorm.
 * User: rafar
 * Date: 24/06/2016
 * Time: 15:03
 */

namespace rrd\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use rrd\Entities\Client;

/**
 * Class ClientRepositoryEloquent
 * @package rrd\Repositories
 */
class ClientRepositoryEloquent extends BaseRepository implements ClientRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Client::class;
    }
}