<?php

namespace rrd\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use rrd\Entities\ProjectNote;

/**
 * Class ProjectNoteRepositoryEloquent
 * @package namespace rrd\Repositories;
 */
class ProjectNoteRepositoryEloquent extends BaseRepository implements ProjectNoteRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return ProjectNote::class;
    }
}
