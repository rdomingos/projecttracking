<?php
/**
 * Created by PhpStorm.
 * User: rafar
 * Date: 29/07/2016
 * Time: 23:26
 */

namespace rrd\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use rrd\Entities\Project;

/**
 * Class ProjectRepositoryEloquent
 * @package rrd\Repositories
 */
class ProjectRepositoryEloquent extends BaseRepository implements ProjectRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Project::class;
    }
}