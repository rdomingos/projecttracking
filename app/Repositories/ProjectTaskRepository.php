<?php

namespace rrd\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface ProjectNoteRepository
 * @package namespace rrd\Repositories;
 */
interface ProjectTaskRepository extends RepositoryInterface
{
    //
}
