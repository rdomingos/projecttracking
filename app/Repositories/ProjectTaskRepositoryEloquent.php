<?php

namespace rrd\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use rrd\Entities\ProjectTask;

/**
 * Class ProjectNoteRepositoryEloquent
 * @package namespace rrd\Repositories;
 */
class ProjectTaskRepositoryEloquent extends BaseRepository implements ProjectTaskRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return ProjectTask::class;
    }
}
