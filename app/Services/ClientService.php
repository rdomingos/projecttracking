<?php
/**
 * Created by PhpStorm.
 * User: rafar
 * Date: 05/07/2016
 * Time: 20:32
 */

namespace rrd\Services;

use Prettus\Validator\Exceptions\ValidatorException;
use rrd\Repositories\ClientRepository;
use rrd\Validators\ClientValidator;

/**
 * Class ClientService
 * @package rrd\Services
 */
class ClientService
{
    /**
     * @var ClientRepository
     */
    protected $repository;
    /**
     * @var ClientValidator
     */
    private $validator;

    /**
     * ClientService constructor.
     * @param ClientRepository $repository
     * @param ClientValidator $validator
     */
    public function __construct(ClientRepository $repository, ClientValidator $validator)
    {
        $this->repository = $repository;
        $this->validator = $validator;
    }

    /**
     * @param array $data
     * @return mixed
     */
    public function create(array $data)
    {
        try {
            $this->validator->with($data)->passesOrFail();
            return $this->repository->create($data);
        } catch (ValidatorException $e) {
            return [
                'success' => false,
                'message' => $e->getMessageBag()
            ];
        }
    }

    /**
     * @param array $data
     * @param $id
     * @return mixed
     */
    public function update(array $data, $id)
    {
        try {
            $this->validator->with($data)->passesOrFail();
            return $this->repository->update($data, $id);
        } catch (ValidatorException $e) {
            return [
                'success' => false,
                'message' => $e->getMessageBag()
            ];
        }
    }

    /**
     * @param $id
     * @return array
     */
    public function delete($id)
    {
        try {
            $this->repository->find($id)->delete();
            return [
                'message' => "Dados de Cliente removidos com sucesso."
            ];
        } catch (ModelNotFoundException $e) {
            return ['error' => true, 'message' => 'Dados de Cliente não encontrado.'];
        } catch (\Exception $e) {
            return ['error' => true, 'message' => 'Ocorreu um erro ao excluir os dados de Cliente.'];
        }
    }
}