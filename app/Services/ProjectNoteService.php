<?php
/**
 * Created by PhpStorm.
 * User: rafar
 * Date: 05/07/2016
 * Time: 20:32
 */

namespace rrd\Services;

use Prettus\Validator\Exceptions\ValidatorException;
use rrd\Repositories\ProjectNoteRepository;
use rrd\Validators\ProjectNoteValidator;

/**
 * Class ProjectService
 * @package rrd\Services
 */
class ProjectNoteService
{
    /**
     * @var ProjectNoteRepository
     */
    protected $repository;
    /**
     * @var ProjectNoteValidator
     */
    private $validator;

    /**
     * ProjectService constructor.
     * @param ProjectNoteRepository $repository
     * @param ProjectNoteValidator $validator
     */
    public function __construct(ProjectNoteRepository $repository, ProjectNoteValidator $validator)
    {
        $this->repository = $repository;
        $this->validator = $validator;
    }

    /**
     * @param array $data
     * @return mixed
     */
    public function create(array $data)
    {
        try {
            //dd($data);
            $this->validator->with($data)->passesOrFail();
            return $this->repository->create($data);
        } catch (ValidatorException $e) {
            return [
                'error' => true,
                'message' => $e->getMessageBag()
            ];
        }
    }

    /**
     * @param array $data
     * @param $project_id
     * @return mixed
     */
    public function update(array $data, $project_id, $note_id)
    {
        try {
            $this->validator->with($data)->passesOrFail();
            if (count($this->repository->findWhere(['project_id' => $project_id, 'id' => $note_id])) > 0) {
                return $this->repository->update($data, $note_id);
            } else {
                return [
                    'error' => true,
                    'message' => "Nota não pertence ao Projeto."
                ];
            }
        } catch (ValidatorException $e) {
            return [
                'error' => true,
                'message' => $e->getMessageBag()
            ];
        }
    }

    /**
     * @param $id
     * @param $project_id
     * @return array
     */
    public function delete($project_id, $note_id)
    {
        try {

            if (count($this->repository->findWhere(['project_id' => $project_id, 'id' => $note_id])) > 0) {
                $this->repository->find($note_id)->delete();
                return [
                    'message' => "Dados de Nota removidos com sucesso."
                ];
            } else {
                return [
                    'error' => true,
                    'message' => "Nota não pertence ao Projeto."
                ];
            }
        } catch (ModelNotFoundException $e) {
            return ['error' => true, 'message' => 'Dados de Nota não encontrado.'];
        } catch (\Exception $e) {
            return ['error' => true, 'message' => 'Ocorreu um erro ao excluir os dados de Nota.'];
        }
    }
}