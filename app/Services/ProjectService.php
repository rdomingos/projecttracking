<?php
/**
 * Created by PhpStorm.
 * User: rafar
 * Date: 05/07/2016
 * Time: 20:32
 */

namespace rrd\Services;

use Illuminate\Database\Eloquent\ModelNotFoundException;
use Prettus\Validator\Exceptions\ValidatorException;
use rrd\Entities\Project;
use rrd\Entities\User;
use rrd\Repositories\ProjectRepository;
use rrd\Validators\ProjectValidator;

/**
 * Class ProjectService
 * @package rrd\Services
 */
class ProjectService
{
    /**
     * @var ProjectRepository
     */
    protected $repository;
    /**
     * @var ProjectValidator
     */
    private $validator;

    /**
     * ProjectService constructor.
     * @param ProjectRepository $repository
     * @param ProjectValidator $validator
     */
    public function __construct(ProjectRepository $repository, ProjectValidator $validator)
    {
        $this->repository = $repository;
        $this->validator = $validator;
    }


    /**
     * @param array $data
     * @return mixed
     */
    public function create(array $data)
    {
        try {
            //dd($data);
            $this->validator->with($data)->passesOrFail();
            return $this->repository->create($data);
        } catch (ValidatorException $e) {
            return [
                'error' => true,
                'message' => $e->getMessageBag()
            ];
        }
    }

    /**
     * @param array $data
     * @param $id
     * @return mixed
     */
    public function update(array $data, $id)
    {
        try {
            $this->validator->with($data)->passesOrFail();
            return $this->repository->update($data, $id);
        } catch (ValidatorException $e) {
            return [
                'error' => true,
                'message' => $e->getMessageBag()
            ];
        }
    }

    /**
     * @param $id
     * @return array
     */
    public function delete($id)
    {
        try {
            $this->repository->find($id)->delete();
            return [
                'message' => "Dados de Projeto removidos com sucesso."
            ];
        } catch (ModelNotFoundException $e) {
            return ['error' => true, 'message' => 'Dados de Projeto não encontrado.'];
        } catch (\Exception $e) {
            return ['error' => true, 'message' => 'Ocorreu um erro ao excluir os dados de Projeto.'];
        }
    }

    /**
     * @param array $data
     * @param $idProjeto
     * @param bool $sync
     * @return array|\Exception|ModelNotFoundException|ValidatorException|NotFoundHttpException
     */
    public function addMembers(array $data, $idProjeto, $sync = false)
    {
        if (isset($data['member_id']) && count($data['member_id']) > 0) {
            $membros = $data['member_id'];
            try {
                try {
                    $project = $this->repository->find($idProjeto);
                } catch (ModelNotFoundException $e) {
                    return ['error' => true, 'message' => 'Dados de Projeto não encontrados.'];
                }

                $isMultiple = gettype($membros) === 'array' ? true : false;


                if ($isMultiple) {
                    if (!$sync) {
                        array_filter($this->isMember($project, $membros),
                            function ($key) use (&$membros) {
                                $membros = array_diff($membros, [$key['user_id']]);
                            }
                        );

                        if ($membros != []) {
                            $project->members()->attach(User::find($membros));
                        }
                    } else {
                        $project->members()->sync(User::find($membros));
                    }
                } else {
                    $isMember = count($this->isMember($project, [$membros])) > 0 ? true : false;

                    if (!$isMember) $project->members()->attach(User::find($membros));
                }

                $project->save();
                return $project->members;
            } catch (NotFoundHttpException $e) {
                return [
                    'error' => true,
                    'message' => $e->getMessageBag()
                ];
            } catch (ValidatorException $e) {
                return $e;
            }
        } else {
            return ['error' => true, 'message' => 'Não há membros para adicionar.'];
        }
    }

    public function removeMember(array $data, $idProjeto)
    {
        if (isset($data['member_id']) && count($data['member_id']) > 0) {
            $membros = $data['member_id'];
            try {
                try {
                    $project = $this->repository->find($idProjeto);
                } catch (ModelNotFoundException $e) {
                    return ['error' => true, 'message' => 'Dados de Projeto não encontrados.'];
                }

                $isMember = count($this->isMember($project, [$membros])) > 0 ? true : false;
                if (!$isMember) {
                    return ['error' => true, 'message' => 'Membro não pertence ao projeto.'];
                }

                $project->members()->detach(User::find($membros));

                $project->save();
                return $project->members;
            } catch (NotFoundHttpException $e) {
                return [
                    'error' => true, 'message' => $e->getMessageBag()
                ];
            } catch (ValidatorException $e) {
                return $e;
            }
        } else {
            return ['error' => true, 'message' => 'Não há membros para remover.'];
        }
    }

    /**
     * @param $member
     * @return array
     */
    public function isMember(Project $projeto, $membros)
    {
        return $projeto->hasMembers($membros);
    }
}