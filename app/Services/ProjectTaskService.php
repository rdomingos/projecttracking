<?php
/**
 * Created by PhpStorm.
 * User: rafar
 * Date: 05/07/2016
 * Time: 20:32
 */

namespace rrd\Services;

use Prettus\Validator\Exceptions\ValidatorException;
use rrd\Repositories\ProjectTaskRepository;
use rrd\Validators\ProjectTaskValidator;

/**
 * Class ProjectService
 * @package rrd\Services
 */
class ProjectTaskService
{
    /**
     * @var ProjectTaskRepository
     */
    protected $repository;
    /**
     * @var ProjectTaskValidator
     */
    private $validator;

    /**
     * ProjectService constructor.
     * @param ProjectTaskRepository $repository
     * @param ProjectTaskValidator $validator
     */
    public function __construct(ProjectTaskRepository $repository, ProjectTaskValidator $validator)
    {
        $this->repository = $repository;
        $this->validator = $validator;
    }

    /**
     * @param array $data
     * @return mixed
     */
    public function create(array $data)
    {
        try {
            $this->validator->with($data)->passesOrFail();
            return $this->repository->create($data);
        } catch (ValidatorException $e) {
            return [
                'error' => true,
                'message' => $e->getMessageBag()
            ];
        }
    }

    /**
     * @param array $data
     * @param $project_id
     * @param $task_id
     * @return mixed
     */
    public function update(array $data, $project_id, $task_id)
    {
        try {
            $this->validator->with($data)->passesOrFail();
            if (count($this->repository->findWhere(['project_id' => $project_id, 'id' => $task_id])) > 0) {
                return $this->repository->update($data, $task_id);
            } else {
                return [
                    'error' => true,
                    'message' => "Tarefa não pertence ao Projeto."
                ];
            }
        } catch (ValidatorException $e) {
            return [
                'error' => true,
                'message' => $e->getMessageBag()
            ];
        }
    }

    /**
     * @param $project_id
     * @param $task_id
     * @return array
     */
    public function delete($project_id, $task_id)
    {
        try {

            if (count($this->repository->findWhere(['project_id' => $project_id, 'id' => $task_id])) > 0) {
                $this->repository->find($task_id)->delete();
                return [
                    'message' => "Dados de Tarefa removidos com sucesso."
                ];
            } else {
                return [
                    'error' => true,
                    'message' => "Nota não pertence ao Projeto."
                ];
            }
        } catch (ModelNotFoundException $e) {
            return ['error' => true, 'message' => 'Dados de Tarefa não encontrado.'];
        } catch (\Exception $e) {
            return ['error' => true, 'message' => 'Ocorreu um erro ao excluir os dados de Tarefa.'];
        }
    }
}