<?php

namespace rrd\Validators;

use \Prettus\Validator\LaravelValidator;

class ProjectTaskValidator extends LaravelValidator
{
    protected $rules = [
        'name' =>'required',
        'project_id' =>'required',
        'status' =>'required'
   ];
}
