<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

$factory->define(rrd\Entities\User::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->name,
        'email' => $faker->safeEmail,
        'password' => bcrypt(str_random(10)),
        'remember_token' => str_random(10),
    ];
});

$factory->define(rrd\Entities\Client::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->name,
        'responsible' => $faker->name,
        'email' => $faker->email,
        'phone' => $faker->phoneNumber,
        'address' => $faker->address,
        'obs' => $faker->sentence
    ];
});

$factory->define(rrd\Entities\Project::class, function (Faker\Generator $faker) {
    return [
        'owner_id' => random_int(1,10),
        'client_id' => random_int(1,10),
        'name' => $faker->word,
        'description' => $faker->sentence,
        'progress' => rand(1,10),
        'status' => random_int(1,3),
        'due_date' => $faker->dateTime('now')
    ];
});

$factory->define(rrd\Entities\ProjectNote::class, function (Faker\Generator $faker) {
    return [
        'project_id' => random_int(1,10),
        'title' => $faker->word,
        'note' => $faker->paragraph
    ];
});

$factory->define(rrd\Entities\ProjectTask::class, function (Faker\Generator $faker) {
    return [
        'name' => implode(" ", $faker->words(random_int(1,3))),
        'project_id' => random_int(1,10),
        'start_date' => $faker->dateTime,
        'due_date' => $faker->dateTime,
        'status' => random_int(1,3),
    ];
});

$factory->define(rrd\Entities\ProjectMember::class, function () {
    return [
        'project_id' => random_int(1,10),
        'user_id' => random_int(1,10),
    ];
});